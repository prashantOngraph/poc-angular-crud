# POC Crud App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Twitter Bootstrap

Run `npm install ngx-bootstrap --save` to generate bootstrap files in your application

Include `./node_modules/bootstrap/dist/css/bootstrap.css` in styles array in `angular.json` file

Include `./node_modules/bootstrap/dist/js/bootstrap.js` in scripts array in `angular.json` file

## Jquery 

Run `npm install jquery --save` to include jquery in your application

Include `./node_modules/jquery/dist/jquery.js` in scripts array in `angular.json` file


# Steps to run project 

1. Clone **Spring-boot-rest-api project** from https://gitlab.com/prashantOngraph/spring-boot-rest-api-test and switch to `test/angular` branch. <br>
2. Run **Spring-boot-rest-api project**<br>
3. Install ng cli
4. Run angular project parallelly
