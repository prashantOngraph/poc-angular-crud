import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddComponent} from "./components/add/add.component";
import {HomeComponent} from "./components/home/home.component";
import {UpdateComponent} from "./components/update/update.component";


const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "book/add", component: AddComponent},
  {path: "book/update/:id", component: UpdateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
