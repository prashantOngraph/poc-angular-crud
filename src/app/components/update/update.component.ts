import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BookService} from "../../services/book.service";
import {IBookResponse} from "../../interfaces/ibook-response";
import {FormBuilder, Validators} from "@angular/forms";
import {Add} from "../../models/add";
import {IAddResponse} from "../../interfaces/iadd-response";

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  public titleRequiredErrorMessage: string = "Title is required";
  public priceRequiredErrorMessage: string = "Price is required";
  public pricePatternErrorMessage: string = "Price range should lie between $100 - $999";
  private _bookId: number;
  public book: IBookResponse;
  public updateResponse: IAddResponse;
  public updateForm = this._formBuilder.group({
    title: [Validators.required],
    price: [[Validators.required, Validators.pattern("^([0-9]{3})?$")]]
  });

  constructor(private _routes: ActivatedRoute,
              private _routeNav: Router,
              private _bookService: BookService,
              private _formBuilder: FormBuilder) {
    /* It will catch parameter passed in url */
    this._routes.params.subscribe(param => this._bookId = param.id)
  }

  ngOnInit(): void {
    this.fetchBookById()
  }

  public fetchBookById() {
    this._bookService.fetchBookById(this._bookId).subscribe(
      response => {
        this.book = response;
      },
      err => {
        console.log("Error: " + err)
      }
    )
  }

  public getTitle() {
    return this.updateForm.get('title')
  }

  public getPrice() {
    return this.updateForm.get('price');
  }

  public update() {
    let model: Add;
    model = this.updateForm.value;
    model.id = this._bookId;
    this._bookService.updateBook(model).subscribe(
      response => {
        this._routeNav.navigate(['']);
      },
      error => {
        this.updateResponse.status = false;
        console.log("Error: " + error)
      }
    );
  }
}
