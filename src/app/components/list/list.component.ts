import {Component, Inject, OnInit} from '@angular/core';
import {BookService} from "../../services/book.service";
import {IListResponse} from "../../interfaces/ilist-response";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";
import {DOCUMENT} from "@angular/common";


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public bookList: IListResponse;
  public closeModal: string;
  public deleteBookName: string;
  public deleteBookId: number;

  constructor(private _bookService: BookService,
              private _routeNav: Router,
              private _modalService: NgbModal,
              @Inject(DOCUMENT) private _document: Document) {
  }

  ngOnInit(): void {
    this._bookService.fetchBooks().subscribe(
      data => {
        this.bookList = data;
      },
      data => {
        this.bookList.status = data.status;
        console.log("Error: " + data)
      }
    )
  }

  open(deleteModalReference, bookTitle, bookId) {
    this.deleteBookName = bookTitle;
    this.deleteBookId = bookId;
    this._modalService.open(deleteModalReference, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeModal = `Closed with: ${result}`;
    }, (reason) => {
      this.closeModal = `Dismiss modal`;
    });
  }

  public deleteBook(modal) {
    this._bookService.deleteBook(this.deleteBookId).subscribe(
      response => {
        modal.close();
        this._document.defaultView.location.reload();
      },
      error => {
        console.log("Error: " + error)
      }
    )
  }

  public isListEmpty(): boolean {
    return !(this.bookList && this.bookList?.data && this.bookList.data.length != 0);
  }


}
