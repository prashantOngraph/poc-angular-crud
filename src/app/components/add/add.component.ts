import {Component, OnInit} from '@angular/core';
import {Add} from "../../models/add";
import {BookService} from "../../services/book.service";
import {IAddResponse} from "../../interfaces/iadd-response";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  public titleRequired: string = "Title is required";
  public priceIsRequired: string = "Price is required";
  public pricePattern: string = "Price range should lie between $100 - $999";
  public bookAddModel = new Add();
  public addResponse: IAddResponse;

  constructor(private _bookService: BookService) {
  }

  ngOnInit(): void {
  }


  public submitHandler(bookForm): void {
    this._bookService.save(this.bookAddModel).subscribe(
      data => {
        this.addResponse = data;
        bookForm.resetForm();
      },
      data => {
        this.addResponse = data;
      }
    )
  }

}
