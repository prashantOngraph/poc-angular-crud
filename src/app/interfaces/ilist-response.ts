import {List} from "../models/list";

export interface IListResponse {

  status: boolean
  data: List[];

}
