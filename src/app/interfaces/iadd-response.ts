export interface IAddResponse {
  status: boolean
  data: string
}
