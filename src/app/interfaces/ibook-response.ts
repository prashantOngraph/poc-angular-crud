import {List} from "../models/list";

export interface IBookResponse {

  status: boolean
  data: List;
}
