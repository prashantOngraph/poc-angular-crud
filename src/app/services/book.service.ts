import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Add} from "../models/add";
import {Observable} from "rxjs";
import {IAddResponse} from "../interfaces/iadd-response";
import {IListResponse} from "../interfaces/ilist-response";
import {IBookResponse} from "../interfaces/ibook-response";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private static _FORM_URL_ENCODED = "application/x-www-form-urlencoded";
  private static _APPLICATION_JSON = "application/json";

  private _BASE_URL: string = "http://localhost:8080";
  private _ADD_URL: string = "/book/add";
  private _LIST_URL: string = "/book/list";
  private _GET_URL: string = "/book/";
  private _UPDATE_URL = "/book/update/";
  private _DELETE_URL = "/book/delete/";

  constructor(private _httpClient: HttpClient) {
  }

  public save(bookModel: Add): Observable<IAddResponse> {
    return this._httpClient.post<IAddResponse>(this._BASE_URL + this._ADD_URL, bookModel)
  }

  public fetchBooks(): Observable<IListResponse> {
    return this._httpClient.get<IListResponse>(this._BASE_URL + this._LIST_URL, {
      headers: {'Content-Type': BookService._FORM_URL_ENCODED}
    })
  }

  public fetchBookById(bookId: number) {
    return this._httpClient.get<IBookResponse>(this._BASE_URL + this._GET_URL + "/" + bookId, {
      headers: {'Content-Type': BookService._FORM_URL_ENCODED}
    })
  }

  public updateBook(bookModel: Add) {
    return this._httpClient.put<IAddResponse>(this._BASE_URL + this._UPDATE_URL + bookModel.id, bookModel, {
      headers: {'Content-Type': BookService._APPLICATION_JSON}
    })
  }

  public deleteBook(bookId: number): Observable<IAddResponse> {
    return this._httpClient.delete<IAddResponse>(this._BASE_URL + this._DELETE_URL + bookId, {
      headers: {'Content-Type': BookService._FORM_URL_ENCODED}
    })
  }
}
